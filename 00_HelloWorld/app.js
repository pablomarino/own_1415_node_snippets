/**
 * NodeJs Helloworld example
 * Created by qojop on 23/07/15.
 */

var http = require('http');

http.createServer(
    // This anonymous function acts as a requestListener with 2 parameters http.serverResponse and http.serverRequest
    function(request,response){
        // response Head
        response.writeHead(200,{'content-type':'html'});
        // response Body
        response.end('<meta charset="utf-8">\n'+
                     '<h1>Hello World</h1>\n');
    }
).listen(8124);

console.log('Server listening on http://localhost:8124/');