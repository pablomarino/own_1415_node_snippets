/**
 * Created by qojop on 24/07/15.
 */
var http = require('http');
var file = require('fs');

http.createServer (
    function (request,response){
        file.readFile('./assets/helloworld.html','utf8',
            function (error, data){
                //
                response.writeHead(200,{'content-type':'html'});
                //
                if (error) response.write("Could'nt read or open the file");
                else response.write(data);
                response.end();
            }
        );
    }
).listen(
    8124,
    function() {
        console.log('Server created, listening on http://localhost:8124/');
    }
);